import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import CircularProgress from '@material-ui/core/CircularProgress'

import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Slider from '@material-ui/core/Slider'
import Button from '@material-ui/core/Button'

import i18nInstance from '../i18n'

import PieChart from '../components/PieChart'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2)
  },
  slider: {
    marginTop: 'calc(45.25px + ' + theme.spacing(2) + 'px)'
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, number, maximum, minimum, requestState } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [value, setValue] = useState(Math.floor((maximum + minimum) / 2))
  const [wait, setWait] = useState(false)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  useEffect(() => {
    setValue(Math.floor((maximum + minimum) / 2))
  }, [minimum, maximum])

  function handleChangeValue(e, value) {
    setValue(value)
  }

  const handleClickSend = () => {
    setWait(true)
    requestState({ event: 'vote', payload: value, successCallback: callbackSend, timeoutCallback: callbackSend })
  }

  const callbackSend = () => {
    setWait(false)
  }

  if (!(locales && maximum !== undefined && minimum !== undefined))
    return <></>

  return (
    <Grid item xs={12} className={classes.items}>
      {number ? (
        <Card>
          <CardContent>
            {t('guest.experiment.waiting_vote_01')}
            <PieChart mode={'vote'} />
          </CardContent>
        </Card>
      ) : (
        <Card>
          <CardContent>
            <Typography variant='body1' component='p'>
              {t('guest.experiment.voting_guide_01', { maximum, minimum })}
            </Typography>
            <Slider
              defaultValue={Math.floor((maximum + minimum) / 2)}
              step={1}
              min={minimum}
              max={maximum}
              valueLabelDisplay='on'
              onChange={handleChangeValue}
              className={classes.slider}
            />
            <CardActions>
              <Button
                variant='contained'
                color='primary'
                onClick={handleClickSend}
                disabled={wait}
              >
                {wait && <CircularProgress size={24} className={classes.buttonProgress}/>}
                {!wait && t('guest.experiment.send_01')}
              </Button>
            </CardActions>
          </CardContent>
        </Card>
      )}
    </Grid>
  )
}
