import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import MobileStepper from '@material-ui/core/MobileStepper'
import Button from '@material-ui/core/Button'
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'

import SwipeableViews from 'react-swipeable-views'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2),
  },
  instructionItem: {
    padding: theme.spacing(2),
    marginBottom: '48px'
  }
}))

export default () => {
  const classes = useStyles()
  const theme = useTheme()
  const { locales, minimum, maximum, denominator, numerator, read, pushState } = useStore()

  const [activeStep, setActiveStep] = React.useState(0)

  const handleNext = () => {
    if (activeStep === instructionItems().length - 2) pushState({ event: 'read' })
    setActiveStep(prevActiveStep => prevActiveStep + 1)
  }

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1)
  }

  const handleSlideStep = value => {
    if (value === instructionItems().length - 1) pushState({ event: 'read' })
    setActiveStep(value)
  }

  const instructionItems = () => {
    const items = t('guest.instruction.instructions_01', { minimum, maximum, denominator, numerator })
    return Array.isArray(items) ? items : ['']
  }
  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
    if (!read && instructionItems().length === 1) pushState({ event: 'read' })
  }, [locales])

  if (!(locales))
    return <></>

  const renderInstructionItems = () => {
    return instructionItems().map((text, i) => (
      <Typography
        key={i}
        variant='body1'
        className={classes.instructionItem}
        align='justify'
        dangerouslySetInnerHTML={{ __html: text }}
      />
    ))
  }

  return (
    <>
      <Grid item>
        <Typography variant='subtitle1' className={classes.items}>
          {t('guest.instruction.instruction_title_01')}
        </Typography>
        <SwipeableViews
          index={activeStep}
          onChangeIndex={handleSlideStep.bind(this)}
        >
          {renderInstructionItems()}
        </SwipeableViews>
        <MobileStepper
          variant="dots"
          steps={instructionItems().length}
          position="bottom"
          activeStep={activeStep}
          className={classes.root}
          nextButton={
            <Button size="small" onClick={handleNext} disabled={activeStep === instructionItems().length - 1}>
              {t('guest.instruction.next_01')}
              {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </Button>
          }
          backButton={
            <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
              {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
              {t('guest.instruction.back_01')}
            </Button>
          }
        />
      </Grid>
    </>
  )
}
