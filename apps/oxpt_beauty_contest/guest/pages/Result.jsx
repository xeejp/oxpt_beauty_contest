import React, { useReducer, useEffect } from 'react'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

import i18nInstance from '../i18n'
import Chart from '../components/Chart'
import Rank from '../components/Rank'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2),
    margin: theme.spacing(2)
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, beautyContestResults, number, maximum, minimum } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && maximum !== undefined && minimum !== undefined && beautyContestResults))
    return <></>

  const numerator = 2; const denominator = 3
  const voters = beautyContestResults.filter(({ _, number }) => number)
  const numbers = voters.map(({ _, number }) => number)
  const sum = numbers.reduce((acc, x) => x + acc)
  const mean = sum / numbers.length
  const standardValue = Math.round((10 * mean * numerator) / denominator, 2) / 10

  const distances = voters.map(({ guestId, number }) => ({
    guestId,
    number: Math.abs(number - standardValue)
  }))
  const sorted = distances.sort((a, b) => a.number - b.number)
  const rank = sorted.findIndex(({ number: n }) => n === Math.abs(number - standardValue)) + 1

  return (
    <Grid item xs={12}>
      <Card className={classes.items}>
        <CardContent>
          <Typography variant='body1' component='p'>
            {t('guest.result.your_rank_01', {
              rank,
              totalNum: numbers.length
            })}
          </Typography>
          {rank === 1 ? (
            <Typography variant='body1' component='p'>
              {t('guest.result.congratlations_01')}
            </Typography>
          ) : null}
          <Typography variant='body1' component='p'>
            {t('guest.result.your_value_01')}: {number}
          </Typography>
          <Typography variant='body1' component='p'>
            {t('guest.result.standard_value_01')}: {standardValue}
          </Typography>
          <Chart
            standardValue={standardValue}
          />
          <Rank standardValue={standardValue} />
        </CardContent>
      </Card>
    </Grid>
  )
}
