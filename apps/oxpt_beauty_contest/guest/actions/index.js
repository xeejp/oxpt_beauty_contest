import { createAction, handleActions } from 'redux-actions'
import { fromSnakeToCamel } from '../util'
import merge from 'deepmerge'

export const defaultState = {
  page: 'instruction'
}

export const PUSH_STATE = 'PUSH_STATE'
export const UPDATE_STATE = 'UPDATE_STATE'
export const REQUEST_STATE = 'REQUEST_STATE'
export const RECEIVE_RESPONSE = 'RECEIVE_RESPONSE'
export const RECEIVE_TIMEOUT = 'RECEIVE_TIMEOUT'
export const pushState = createAction(PUSH_STATE)
export const updateState = createAction(UPDATE_STATE)
export const requestState = createAction(REQUEST_STATE)
export const receiveResponse = createAction(RECEIVE_RESPONSE)
export const receiveTimeout = createAction(RECEIVE_TIMEOUT)

export const reducer = handleActions(
  {
    [PUSH_STATE]: (state, _action) => state,
    [UPDATE_STATE]: (state, action) => {
      return merge(state, fromSnakeToCamel(action.payload), { arrayMerge: (_destArr, srcArr) => srcArr })
    },
    [REQUEST_STATE]: (state, _action) => state,
    [RECEIVE_RESPONSE]: (state, action) => {
      if (action.payload) {
        return merge(state, fromSnakeToCamel(action.payload), { arrayMerge: (_destArr, srcArr) => srcArr })
      } else {
        return state
      }
    },
    [RECEIVE_TIMEOUT]: (state, _action) => state
  },
  defaultState
)
