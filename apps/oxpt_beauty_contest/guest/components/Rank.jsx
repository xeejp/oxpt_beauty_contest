import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(2)
  },
  rankTable: {
    textAlign: 'center'
  },
  myNumber: {
    backgroundColor: '#FFBABA'
  }
}))

const RankTable = ({ data, index, classes, t }) => (
  <Grid item xs={12}>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>{t('guest.result.rank.rank_01')}</TableCell>
          <TableCell>{t('guest.result.rank.number_01')}</TableCell>
          <TableCell>{t('guest.result.rank.distance_01')}</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {data
          .filter(a => a.rank <= 10)
          .map((a, i) =>
            i === index ? (
              <TableRow key={a.key} className={classes.myNumber}>
                <TableCell>{a.rank + '(You)'}</TableCell>
                <TableCell>{a.number}</TableCell>
                <TableCell>{Math.round(a.value * 10) / 10}</TableCell>
              </TableRow>
            ) : (
              <TableRow key={a.key}>
                <TableCell>{a.rank}</TableCell>
                <TableCell>{a.number}</TableCell>
                <TableCell>{Math.round(a.value * 10) / 10}</TableCell>
              </TableRow>
            )
          )}
        {index !== -1 && data[index] && data[index].rank > 10 ? (
          <TableRow className={classes.myNumber}>
            <TableCell>{data[index].rank + '(You)'}</TableCell>
            <TableCell>{data[index].number}</TableCell>
            <TableCell>{Math.round(data[index].value * 10) / 10}</TableCell>
          </TableRow>
        ) : null}
      </TableBody>
    </Table>
  </Grid>
)

const Rank = ({ standardValue }) => {
  const classes = useStyles()
  const { locales, beautyContestResults, number: myNumber } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && beautyContestResults))
    return <></>

  const votedPlayer = Object.keys(beautyContestResults) // Object.keys(beautyContestResults).filter(key => beautyContestResults[key].voted)

  let data = votedPlayer
    .map(key => ({
      value: Math.abs(beautyContestResults[key].number - standardValue),
      number: beautyContestResults[key].number,
      key: key,
      rank: 1
    }))
    .sort((a, b) => {
      if (a.value < b.value) return -1
      if (a.value > b.value) return 1
      if (a.number > b.number) return -1
      return 1
    })

  let f = true; let index
  if (myNumber === 'host') {
    f = false
    index = -1
  }

  if (myNumber !== null && f && data[0].number === myNumber) {
    f = false
    index = 0
  }

  for (let i = 1; i < data.length; i++) {
    if (data[i - 1].value === data[i].value) {
      data[i].rank = data[i - 1].rank
    } else {
      data[i].rank = i + 1
    }

    if (myNumber !== null && f && myNumber === data[i].number) {
      f = false
      index = i
    }
  }
  return (
    <RankTable
      className={classes.rankTable}
      data={data}
      index={index}
      classes={classes}
      t={t}
    />
  )
}

RankTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  index: PropTypes.number,
  classes: PropTypes.object,
  t: PropTypes.func
}

Rank.propTypes = {
  standardValue: PropTypes.number
}

export default Rank
