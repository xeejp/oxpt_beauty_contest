import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'

const PieChart = ({ mode }) => {
  const { locales, playerNumber, votedNumber, readNumber } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && playerNumber !== undefined && votedNumber !== undefined))
    return <></>

  const name =
    mode === 'vote'
      ? [t('guest.result.chart.voting_01'), t('guest.result.chart.voted_01')]
      : [t('guest.result.chart.read_01'), t('guest.result.chart.reading_01')]

  let plotOptions = {
    dataLabels: {
      enabled: true,
      format: '<b>{point.name}</b>: {point.y}'
    }
  }

  let data = []

  if (mode === 'vote') {
    Object.assign(plotOptions, {
      startAngle: -90,
      endAngle: 90,
      center: ['50%', '75%']
    })

    const unvotedNumber = playerNumber - votedNumber

    data = [
      {
        name: name[1],
        y: votedNumber,
        color: '#7cb5ec'
      },
      {
        name: name[0],
        y: unvotedNumber,
        color: '#f7a35c'
      }
    ]
  } else {
    const unreadNumber = playerNumber - readNumber

    data = [
      {
        name: name[0],
        y: readNumber,
        color: '#7cb5ec'
      },
      {
        name: name[1],
        y: unreadNumber,
        color: '#f7a35c'
      }
    ]
  }

  return (
    <HighchartsReact
      options={{
        title: {
          text: ''
        },
        tooltip: {
          pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
          pie: plotOptions
        },
        series: [
          {
            type: 'pie',
            innerSize: '50%',
            data: data
          }
        ],
        credits: {
          enabled: false
        }
      }}
      highcharts={Highcharts}
    />
  )
}

PieChart.propTypes = {
  mode: PropTypes.string
}

export default PieChart
