import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

import i18nInstance from '../i18n'

const Chart1 = ({ data, standardValue, avr }) => {
  const { locales, maximum, minimum } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales && maximum !== undefined && minimum !== undefined))
    return <></>

  const width = Math.round((maximum - minimum + 1) / 10)
  var minData = new Array(10).fill(0)
  const categories = minData.map((_, index) => [
    minimum + index * width,
    index === 9 ? maximum : minimum + (index + 1) * width - 1
  ])

  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < width; j++) {
      const index = i * width + j
      if (maximum - minimum >= index) {
        minData[i] += data[index]
      }
    }
  }

  for (let j = minimum + 9 * width - minimum; j < maximum - minimum + 1; j++) {
    minData[9] += data[j]
  }

  return (
    <HighchartsReact
      options={{
        chart: {
          type: 'column'
        },
        credits: {
          enabled: false
        },
        title: {
          text: null
        },
        plotOptions: {
          series: {
            pointPadding: 0,
            groupPadding: 0,
            borderWidth: 0,
            shadow: false
          }
        },
        xAxis: {
          title: {
            text: null
          },
          minimum: 0,
          categories: categories.map(a => a[0] + '-' + a[1]),
          plotLines: [
            {
              color: '#FF0000',
              width: 2,
              value: (standardValue - minimum) / width - 0.5,
              zIndex: 6,
              label: {
                useHTML: true,
                text:
                  t('guest.result.standard_value_01') +
                  '<br />' +
                  Math.round(standardValue * 10) / 10, // standerd
                verticalAlign: 'top',
                rotation: 0,
                y: 13
              }
            },
            {
              color: '#00FF00',
              width: 2,
              value: (avr - minimum) / width - 0.5,
              zIndex: 5,
              label: {
                useHTML: true,
                text:
                  t('guest.result.chart.mean_value_01') +
                  '<br />' +
                  Math.round(avr * 10) / 10, // average
                verticalAlign: 'top',
                rotation: 0,
                y: 39
              }
            }
          ]
        },
        yAxis: {
          title: {
            text: t('guest.result.chart.number_of_votes_01')
          },
          minimum: 0,
          allowDecimals: false
        },
        series: [
          {
            name: t('guest.result.chart.number_of_votes_01'),
            data: minData
          }
        ]
      }}
      highcharts={Highcharts}
    />
  )
}

Chart1.propTypes = {
  data: PropTypes.arrayOf(PropTypes.number),
  standardValue: PropTypes.number,
  avr: PropTypes.number
}

export default Chart1
