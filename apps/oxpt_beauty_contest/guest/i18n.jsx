import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    debug: process.env.NODE_ENV === `development`,

    interpolation: {
      escapeValue: false // not needed for react!!
    },

    resources: {
      en: { translations: [] },
      ja: { translations: [] }
    },

    react: {
      wait: true,
      useSuspense: false
    },

    returnObjects: true
  })

export default i18n
