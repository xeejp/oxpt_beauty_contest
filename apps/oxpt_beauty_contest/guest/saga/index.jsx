import 'babel-polyfill'
import { takeEvery, put, call } from 'redux-saga/effects'
import { eventChannel } from 'redux-saga'
import channel from 'oxpt'
import * as Actions from '../actions'
import { fromCamelToSnake } from '../util'

function subscribe() {
  return eventChannel(emit => {
    channel.on('update_state', ({ state }) => {
      emit(Actions.updateState(state))
    })
    return () => { }
  })
}

function * receiveSocket(action) {
  yield put(action)
}

function * sendData(action) {
  const { event, payload } = action.payload
  if (payload === undefined || payload === null) {
    channel.push('input', {
      event: event,
      payload: null
    })
  } else {
    channel.push('input', {
      event: event,
      payload: fromCamelToSnake(payload)
    })
  }
}

function * request(action) {
  const { event, payload, successCallback, timeoutCallback, timeout, dispatch } = {
    payload: {},
    successCallback: () => {},
    timeoutCallback: () => {},
    timeout: 1000,
    ...action.payload,
  }

  channel
    .push('request', {
      event: event,
      payload: fromCamelToSnake(payload),
      timeout: timeout,
    })
    .receive('ok', res => {
      dispatch(Actions.receiveResponse(res))
      successCallback(res)
    })
    .receive('timeout', (res) => {
      dispatch(Actions.receiveTimeout(res))
      timeoutCallback()
    })
}

export default function * root() {
  const chan = yield call(subscribe)
  yield takeEvery(chan, receiveSocket)
  yield takeEvery(Actions.PUSH_STATE, sendData)
  yield takeEvery(Actions.REQUEST_STATE, request)
}
