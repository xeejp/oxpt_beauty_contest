import React from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'

import Chart1 from './Chart1'
import Chart2 from './Chart2'

const Chart = ({ standardValue }) => {
  const { players, maximum, minimum } = useStore()

  if (!(maximum !== undefined && minimum !== undefined))
    return <></>

  const data = Array(maximum - minimum + 1).fill(0)

  let inputs = 0
  let sum = 0

  players.forEach(player => {
    if (player.number) {
      data[player.number - minimum]++
      inputs++
      sum += player.number
    }
  })

  const avr = sum / inputs

  return inputs < 100 ? (
    <Chart1
      data={data}
      standardValue={standardValue}
      avr={avr}
    />
  ) : (
    <Chart2
      data={data}
      standardValue={standardValue}
      avr={avr}
    />
  )
}

Chart.propTypes = {
  standardValue: PropTypes.number
}

export default Chart
