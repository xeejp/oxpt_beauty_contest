import React, { useReducer, useEffect } from 'react'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import i18nInstance from './i18n'
import Stepper from './components/Stepper'
import GuestTable from './components/GuestTable'

const useStyles = makeStyles(theme => ({
  title: {
    padding: 0,
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1),
  },
  items: {
    paddingTop: theme.spacing(2),
    paddingRight: 0,
    paddingLeft: 0
  },
}))

export default () => {
  const classes = useStyles()
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <>
      <Grid item xs={12} className={classes.title}>
        <Typography variant="h5">
          {t('host.title_01')}
        </Typography>
      </Grid>
      <Grid item xs={12} className={classes.items}>
        <Stepper />
      </Grid>
      <Grid item xs={12} className={classes.items}>
        <GuestTable />
      </Grid>
    </>
  )
}
