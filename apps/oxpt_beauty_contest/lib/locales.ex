defmodule Oxpt.BeautyContest.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{},
          host: %{
            title_01: "Beauty Contest Game",
            stepper: %{
              instruction_page_01: "Instruction",
              experiment_page_01: "Experiment",
              result_page_01: "Result",
              back_01: "Back",
              next_01: "Next"
            },
            setting: %{},
            guest_table: %{}
          },
          guest: %{
            instruction: %{
              instruction_title_01: "Instruction",
              next_01: "next",
              back_01: "back",
              instructions_01: [
                "<p>Now you will play numbers game.</p><p>Please select an integer number between {{minimum}} to {{maximum}}.</p><p>The person who entered number which is the nearest of {{numerator}}\/{{denominator}} of mean."
              ]
            },
            experiment: %{
              waiting_vote_01: "Please wait for voting.",
              voting_guide_01:
                "Please select an integer number to vote between {{minimum}} to {{maximum}} (including {{minimum}} and {{maximum}}).",
              send_01: "Send"
            },
            result: %{
              your_rank_01: "You are {{rank}} of {{totalNum}}.",
              congratlations_01: "Congratlations!",
              your_value_01: "Your value",
              standard_value_01: "Standard value",
              chart: %{
                voting_01: "Voting",
                voted_01: "Voted",
                read_01: "read",
                reading_01: "reading",
                mean_value_01: "Mean value",
                number_of_votes_01: "Number of votes"
              },
              rank: %{
                rank_01: "Rank",
                number_01: "Number",
                distance_01: "Distance"
              }
            }
          }
        }
      },
      ja: %{
        translations: %{
          variables: %{},
          host: %{
            title_01: "美人投票ゲーム",
            stepper: %{
              instruction_page_01: "説明",
              experiment_page_01: "実験",
              result_page_01: "結果",
              back_01: "戻る",
              next_01: "次へ"
            },
            setting: %{},
            guest_table: %{}
          },
          guest: %{
            instruction: %{
              instruction_title_01: "ゲームの説明",
              next_01: "次へ",
              back_01: "戻る",
              instructions_01: [
                "<p>数当てゲームをしましょう。</p><p>{{minimum}}から{{maximum}}までの整数から数字を選んでください。</p><p>全体の平均の{{numerator}}/{{denominator}}に最も近い数字を答えた人が優勝です。</p>"
              ]
            },
            experiment: %{
              waiting_vote_01: "投票が終わるまでお待ちください。",
              voting_guide_01:
                "{{minimum}}から{{maximum}}までの整数から数字を選んでください({{minimum}}や{{maximum}}も選べます)。",
              send_01: "送信"
            },
            result: %{
              your_rank_01: "あなたは{{totalNum}}人中{{rank}}位でした。",
              congratlations_01: "おめでとうございます！",
              your_value_01: "あなたの数字",
              standard_value_01: "基準値",
              chart: %{
                voting_01: "投票中",
                voted_01: "投票済み",
                read_01: "完読",
                reading_01: "読んでいる最中",
                mean_value_01: "平均値",
                number_of_votes_01: "投票数"
              },
              rank: %{
                rank_01: "順位",
                number_01: "投票した数字",
                distance_01: "基準値との差"
              }
            }
          }
        }
      }
    }
  end
end
