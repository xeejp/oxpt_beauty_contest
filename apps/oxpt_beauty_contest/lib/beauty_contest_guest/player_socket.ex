defmodule Oxpt.BeautyContest.Guest.PlayerSocket do
  defstruct [:game_id, :guest_id, :channel_pid]

  use Oxpt.PlayerSocket

  alias Cizen.{Dispatcher, Filter, Event}
  alias Oxpt.Player.{Input, Output, Request}

  alias Oxpt.BeautyContest.Events.{
    UpdateState,
    UpdateStateAll,
    FetchState,
    Read,
    Vote
  }

  require Filter

  @impl GenServer
  def init(%__MODULE__{game_id: game_id, guest_id: guest_id} = socket) do
    Dispatcher.listen(
      Filter.any([
        Filter.new(fn %Event{body: %UpdateStateAll{game_id: ^game_id}} -> true end),
        Filter.new(fn %Event{body: %UpdateState{game_id: ^game_id, guest_id: ^guest_id}} ->
          true
        end)
      ])
    )

    Dispatcher.dispatch(
      Event.new(
        nil,
        %FetchState{
          game_id: game_id,
          guest_id: guest_id
        }
      )
    )

    state = %{
      throttled?: false,
      updated?: false,
      state: %{}
    }

    {:ok, socket |> Map.from_struct() |> Map.merge(state)}
  end

  @impl GenServer
  def handle_info(%Event{body: %event_type{event: event, state: next_state}}, state)
      when event_type in [UpdateStateAll, UpdateState] do
    state = update_in(state.state, &Map.merge(&1, get_merge_data(next_state, state.guest_id)))
    state = put_in(state.updated?, true)
    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info(:end_of_throttled_interval, state) do
    state = put_in(state.throttled?, false)

    {:noreply, state, {:continue, :try_send}}
  end

  @impl GenServer
  def handle_info({:response, _, _}, state), do: {:noreply, state}

  @impl GenServer
  def handle_continue(:try_send, state) do
    state =
      if state.updated? and not state.throttled? do
        send(
          state.channel_pid,
          %Output{
            event: "update_state",
            payload: %{state: state.state}
          }
        )

        state = put_in(state.throttled?, true)
        state = put_in(state.updated?, false)
        # 30fps
        Process.send_after(self(), :end_of_throttled_interval, 33)
        state
      else
        state
      end

    {:noreply, state}
  end

  @impl GenServer
  def handle_cast(%Input{event: "read"}, state) do
    Dispatcher.dispatch(
      Event.new(nil, %Read{
        game_id: state.game_id,
        guest_id: state.guest_id
      })
    )

    {:noreply, state}
  end

  def handle_call(%Request{event: "vote", payload: number, timeout: timeout}, from, state) do
    request_id = {self(), from}

    Dispatcher.dispatch(
      Event.new(nil, %Vote{
        game_id: state.game_id,
        guest_id: state.guest_id,
        request_id: request_id,
        number: number
      })
    )

    receive do
      {:response, ^request_id, payload} ->
        {:reply, payload, state}
    after
      timeout ->
        {:noreply, state}
    end
  end

  defp get_merge_data(state, guest_id) do
    player = get_in(state, [:players, guest_id]) || %{}
    group = get_in(state, [:groups, get_in(player, [:group_id])])

    state
    |> Map.drop([:players, :groups])
    |> Map.merge(player)
  end
end
