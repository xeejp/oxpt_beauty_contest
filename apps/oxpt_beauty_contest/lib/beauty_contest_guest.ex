defmodule Oxpt.BeautyContest.Guest do
  @moduledoc """
  Documentation for Oxpt.BeautyContest.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game
  alias Oxpt.Player
  alias Oxpt.Lounge.Host.AddedDummyGuest

  alias Oxpt.BeautyContest.{Host, Locales}

  alias Oxpt.BeautyContest.Events.{
    FetchState,
    UpdateState,
    UpdateStateAll,
    ChangePage,
    Read,
    Vote
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_beauty_contest",
      category: "category_game_theory"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(id, %Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %JoinGame{game_id: ^id}} -> true end)
    })

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %AddedDummyGuest{game_id: ^id}
          } ->
            true

          %Event{
            body: %ChangePage{game_id: ^id}
          } ->
            true

          %Event{
            body: %Read{game_id: ^id}
          } ->
            true

          %Event{
            body: %Vote{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      locales: Locales.get(),
      page: "instruction",
      players: %{},
      player_number: 0,
      read_number: 0,
      voted_number: 0,
      minimum: 1,
      maximum: 100,
      denominator: 3,
      numerator: 2,
      dummy_guest_list: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state = handle_event_body(id, event.body, state)
    {:loop, state}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    perform(id, %Request{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    state
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    new_state = put_in(new_state, [:player_number], map_size(new_state.players))

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players]),
          player_number: get_in(new_state, [:player_number])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateState{
        game_id: id,
        guest_id: guest_id,
        state: state
      }
    })

    state
  end

  defp handle_event_body(id, %AddedDummyGuest{dummy_guest_list: dummy_guest_list}, state) do
    new_state = put_in(state, [:dummy_guest_list], dummy_guest_list)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          dummy_guest_list: get_in(new_state, [:dummy_guest_list])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %ChangePage{request_id: request_id, page: page}, state) do
    new_state =
      state
      |> Map.put(:page, page)

    new_state =
      if page == "experiment" && state.page == "instruction" do
        # playersとvoted_numberとread_numberを変更
        reset(new_state)
      else
        new_state
      end
      |> set_status(page)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: new_state
      }
    })

    response(id, request_id)

    new_state
  end

  defp handle_event_body(id, %Read{guest_id: guest_id}, state) do
    new_state = put_in(state, [:players, guest_id, :read], true)

    read_number =
      new_state.players
      |> Enum.filter(fn {_, %{read: read}} -> read end)
      |> Enum.into(%{})
      |> map_size()

    new_state = put_in(new_state, [:read_number], read_number)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players]),
          read_number: get_in(new_state, [:read_number])
        }
      }
    })

    new_state
  end

  defp handle_event_body(
         id,
         %Vote{request_id: request_id, guest_id: guest_id, number: number},
         state
       ) do
    new_state =
      state
      |> put_in([:players, guest_id, :number], number)
      |> put_in([:players, guest_id, :status], "voted")
      |> put_in([:players, guest_id, :finished], true)

    voted_number =
      new_state.players
      |> Enum.filter(fn {_, %{number: number}} -> not is_nil(number) end)
      |> Enum.into(%{})
      |> map_size()

    beauty_contest_results =
      new_state.players
      |> Enum.map(fn {guest_id, player} ->
        %{guestId: guest_id, number: player.number}
      end)

    new_state =
      new_state
      |> put_in([:voted_number], voted_number)
      |> put_in([:beauty_contest_results], beauty_contest_results)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          voted_number: get_in(new_state, [:voted_number]),
          players: get_in(new_state, [:players])
        }
      }
    })

    if new_state.voted_number === new_state.player_number do
      perform(id, %Dispatch{
        body: %ChangePage{
          game_id: id,
          request_id: request_id,
          page: "result"
        }
      })
    end

    response(id, request_id)

    new_state
  end

  defp reset(state) do
    # playersとvoted_numberとread_numberを変更

    players =
      Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players ->
        acc_players
        |> update_in([key], fn player ->
          player |> Map.merge(new_player(state))
        end)
      end)

    state
    |> Map.merge(%{
      players: players,
      voted_number: 0,
      read_number: 0
    })
  end

  defp set_status(state, page) do
    new_players =
      Enum.reduce(Map.keys(state.players), state.players, fn key, acc_players ->
        acc_players
        |> update_in([key], fn player ->
          player
          |> Map.merge(%{
            status:
              case page do
                "experiment" -> "voting"
                _ -> nil
              end
          })
        end)
      end)

    state
    |> Map.merge(%{
      players: new_players
    })
  end

  defp new_player(_state) do
    %{
      read: false,
      status: nil,
      number: nil,
      finished: false
    }
  end

  defp response(id, request_id, status \\ :ok, payload \\ %{}) do
    send(elem(request_id, 0), {:response, request_id, payload})
    :ok
  end
end
